# Choose one (ordered in difficulty)

0. Have a look at apiExample.R and understand it
1. Complement the api Example with a way to extract the collections meaningfully 
2. Write an efficient function in Go to check whether a string is a permutation of a palindrome 
3. Implement a Stack or a Queue in Go